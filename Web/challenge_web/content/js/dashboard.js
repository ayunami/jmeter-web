/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.45466666666666666, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.44, 500, 1500, "POST products.json"], "isController": false}, {"data": [0.655, 500, 1500, "GET categories/ID.json"], "isController": false}, {"data": [0.0075, 500, 1500, "PUT products/ID.json"], "isController": false}, {"data": [0.595, 500, 1500, "PUT offers/ID.json"], "isController": false}, {"data": [0.5775, 500, 1500, "GET products/ID.json"], "isController": false}, {"data": [0.58, 500, 1500, "GET users/{user_id}/offers.json"], "isController": false}, {"data": [0.335, 500, 1500, "POST users.json"], "isController": false}, {"data": [0.115, 500, 1500, "PUT profiles.json"], "isController": false}, {"data": [0.6275, 500, 1500, "GET categories.json"], "isController": false}, {"data": [0.4, 500, 1500, "POST users/sign_in.json as buyer"], "isController": false}, {"data": [0.5725, 500, 1500, "POST offers.json"], "isController": false}, {"data": [0.5925, 500, 1500, "DELETE products/ID.json"], "isController": false}, {"data": [0.515, 500, 1500, "GET products.json"], "isController": false}, {"data": [0.285, 500, 1500, "POST users/sign_in.json"], "isController": false}, {"data": [0.5225, 500, 1500, "GET profiles.json"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 3000, 0, 0.0, 1252.3249999999994, 43, 12363, 1008.5, 2076.9, 3084.8499999999995, 6149.999999999978, 15.415525489571397, 37.42605064032239, 1049.8705898750827], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["POST products.json", 200, 0, 0.0, 1133.67, 413, 2320, 1103.5, 1684.1000000000001, 1939.35, 2209.3900000000003, 1.112811239393518, 2.342478526220615, 6.73481186534984], "isController": false}, {"data": ["GET categories/ID.json", 200, 0, 0.0, 692.0500000000003, 52, 2462, 666.0, 1271.7, 1513.3499999999997, 1798.97, 1.114647019155209, 0.7989755000585189, 0.19702256881552035], "isController": false}, {"data": ["PUT products/ID.json", 200, 0, 0.0, 4127.295000000001, 1321, 12363, 3462.5, 7039.3, 10223.69999999999, 11891.730000000003, 1.1063537898149072, 2.307855085977519, 1112.7231517754904], "isController": false}, {"data": ["PUT offers/ID.json", 200, 0, 0.0, 763.7750000000001, 65, 1745, 772.0, 1211.7, 1387.9, 1624.5400000000004, 1.1190125832964992, 3.3547210441506414, 0.5431145057601172], "isController": false}, {"data": ["GET products/ID.json", 200, 0, 0.0, 800.4950000000002, 52, 1944, 774.5, 1307.6, 1423.1999999999998, 1818.89, 1.117505727216852, 1.857776879504945, 0.19971049617254288], "isController": false}, {"data": ["GET users/{user_id}/offers.json", 200, 0, 0.0, 790.4200000000001, 49, 1726, 757.0, 1326.9, 1460.6499999999999, 1672.1700000000008, 1.119100243404303, 1.232529358895448, 0.4743061578490893], "isController": false}, {"data": ["POST users.json", 200, 0, 0.0, 1399.6499999999996, 335, 3163, 1362.5, 1979.7, 2103.5499999999997, 2574.82, 1.0630212125882972, 1.8227336138602022, 0.32804170232216984], "isController": false}, {"data": ["PUT profiles.json", 200, 0, 0.0, 2033.6149999999984, 902, 4799, 1846.5, 3348.7000000000007, 3922.349999999999, 4596.120000000001, 1.0716046207591248, 1.4980279126213591, 6.27733742920712], "isController": false}, {"data": ["GET categories.json", 200, 0, 0.0, 739.3149999999999, 43, 1772, 736.5, 1293.2, 1480.2999999999993, 1702.5500000000004, 1.113920666570127, 1.1639600715137068, 0.19471855401958274], "isController": false}, {"data": ["POST users/sign_in.json as buyer", 200, 0, 0.0, 1236.34, 326, 2638, 1295.5, 1707.7, 1903.9, 2399.59, 1.1172310545543924, 2.237844351559096, 0.31749437194856267], "isController": false}, {"data": ["POST offers.json", 200, 0, 0.0, 851.415, 97, 1768, 875.0, 1329.0, 1463.1, 1740.5100000000014, 1.1188185276348177, 3.3618639341855, 0.5867241692772432], "isController": false}, {"data": ["DELETE products/ID.json", 200, 0, 0.0, 784.3949999999999, 50, 1869, 784.5, 1330.9, 1438.0, 1773.6400000000012, 1.1193319826727408, 0.9614558906244753, 0.49408013297663955], "isController": false}, {"data": ["GET products.json", 200, 0, 0.0, 944.2200000000003, 119, 1942, 895.0, 1473.9, 1541.2499999999998, 1742.6600000000003, 1.1143613316617913, 14.054991119933138, 0.23070761944560525], "isController": false}, {"data": ["POST users/sign_in.json", 200, 0, 0.0, 1512.2400000000002, 349, 3165, 1442.0, 2458.4000000000005, 3087.85, 3158.83, 1.063937312813529, 1.8230472344944437, 0.3033883743569829], "isController": false}, {"data": ["GET profiles.json", 200, 0, 0.0, 975.9800000000004, 46, 4119, 869.5, 1650.9, 1945.4499999999994, 3449.880000000003, 1.089502642043907, 1.5223352297488697, 0.45112218772130525], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 3000, 0, "", "", "", "", "", "", "", "", "", ""], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
